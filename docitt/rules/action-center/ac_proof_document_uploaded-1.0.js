function ac_proof_document_uploaded(request, event) {
	if (request == null) {
		return false;
	};
	if (event == null || event.Data == null || event.Data.Document == null || event.Data.Document.Metadata == null) {
		return false;
	};
	if (request.Id === event.Data.Document.Metadata.requestId) {
		return true;
	};
	return false;
}

/*
BODY :
	{
		"request" : {
			"Id" : 1
		},
		"event" : {
			"Data" : {
				"Document" : {
					"Metadata" : {
						"requestId" : 1
					}
				}
			}
		}
	}
*/